## \mainpage Mechatronics
# This portfolio contains the projects written by Matthew Nagy during ME305, 
# Winter 2021
#
#
#
## @defgroup group1 Lab 1   
# 
## \addtogroup group1
# @{   
#      @page lab1 
#      \section sec1 The Code for Lab 1
#       The first lab consisted of a simple Fibonnaci counter that would give
#       the value the selected fibonacci index. For further information, follow
#       the files below.
#       All of the files for Lab 1  
#      \ref thefibonacci.py   
#  @}   
# 
## @defgroup group2 Lab 2   
# 
## \addtogroup group2
# @{   
#      @page lab2   
#      \section sec2 The Code for Lab 2 
#      The second lab consisted of a light pattern that was changeable by the 
#      user pressing a button on the nucleo. For further information and FSM 
#      diagrams, follow the file below.
#       \ref LAB0x02.py     
#  @}   
# 
## @defgroup group3 HW 2
# 
## \addtogroup group3
# @{   
#      @page hw3   
#       \section sec3 The Code for Homework 2 
#       The second homeowrk consisted of making an elevator simulator in FSM
#       format. For further information and the FSM diagram, follow the files
#       below.   
#       \subsection HW3S1 The Code
#       \ref Homework0x02.py    
#  @} 
#  
## @defgroup group4 Lab 3
# 
## \addtogroup group4
# @{   
#      @page lab3  
#      \section sec4 The Code for Lab 3 
#      Lab 3 consisted of a "Simon Says" style game, where the user would need 
#      to replicate the pattern shown on the board LED. For further information
#      follow the links below.
#     \ref LightPattern.py
#       \ref SimonSays.py
#  @}
#   
## @defgroup group5 Final Project Week 1   
# 
## \addtogroup group5
# @{   
#     @page FPW1   
#      \section sec5 Week 1 of the Final Project
#      This page contains the finite state machine diagram for the Nucleo code,
#      the task diagram and an image of the output graph.
#      
#       Week 1 consisted of developing the interface between the nucleo and the
#       PC. This was done through serial connection, where the PC side terminal
#       sends the data collection command, and the Nucleo starts collection.
#       After this, the nucleo sends the data back to the terminal where it is graphed
#       and a csv is created.
#       
#      \subsection  The Fininte State Machine Diagram for the Nucleo
# @image html wk1FSM.jpg [This is the FSM for the Nucleo]     
#      \subsection  The Task Diagram for the System 
# @image html wk1task.jpg [This is the task diagram for the system of week 1]
#      \subsection  The Output Graph of Week 1  
# @image html wk1graph.jpg [This is the output graph for week 1]
#      \subsection  LL Links to Code Referenced
#      \ref lab4.py
#      \ref lab4terminalsecond.py  
#  @}   
# 
#
## @defgroup group6 Final Project Week 2
# 
## \addtogroup group6
# @{   
#      @page FPW2
#      \section sec6  Week 2 of the Final Project 
#      This page contains the finite state machine diagram for the Nucleo code,
#      the task diagram and an image of the output graph for week 2 of the final 
#      project.
#
#       Week 2 of the final project consisted of upgrading some of the functionality
#       of week 1. This came primarily in the form of encoder support. Through the 
#       use of an encoder driver paired with an encoder task, the user could 
#       input a number of commands, ranging from resetting the position of the 
#       encoder, getting its current position, or getting the change in position.
#       This was paired with a PC side component that allows for graphing and user
#       interface. To explore the code and documented functionality further, 
#       please follow the code links shown below. 
#      
#      \subsection  The output of week 2 code.
# @image html wk2FSM.jpg [This is the finite state machine drawing for the encoder task in week 2 of the final project]
#      \subsection  The Finite State Drawing for the encoder task.
# @image html wk2task.jpg [This is the task diagram in week 2 of the final project]
#      \subsection  The Finite State Drawing for the UI task
# @image html wk2graph.JPG [This is the output graph for week 2 of the final project]
#      \subsection  L2 Links to Code Referenced
# \ref encoder1.py
# \ref encodermain.py
# \ref EncoderUI.py
#  @}   
#
## \defgroup group7 Final Project Week 3
# 
## \addtogroup group7
# @{   
#      @page FPW3 
#      \section sec7  Week 3 of the Final Project 
#      This page contains 
#
#       Week 3 of the final project implemented some much needed upgrades to the 
#       system. By combining the encoder with a now functioning motor and controller
#       driver, motor control was enabled that would allow for speed control.
#       The controller I implimented was a step based controller that took the 
#       difference between the old speed and the required speed, and multiplied
#       this by Kp. With this method, the response rate of the motor to the speed
#       requested could be adjusted by changing Kp.  
#
#       The optimal Kp for my system turned out to be about 0.001, with the 
#       increases of Kp making the system oscillate more, and if the Kp was
#       raised too high, it would exceed the RPM requested greatly as shown
#       in the figures below. 
#
#       I ran into a few issues with the hardware that led to limited access to 
#       test hardware. After spending a few days troubleshooting both hardware
#       as well as software changes such as sampling rate, print statements, and
#       anything I could come up with that would have a chance to interfere, I 
#       ended up with an encoder that would loose position. The symptoms would be
#       an encoder that would only report a position of zero, while the code would
#       run fine on a friends hardware. Due to these limitiations and not wanting 
#       to keep asking for code to be run, I only had a little but of time to 
#       optimise the code, and I did not have time to complete and test an intergral
#       controller which would have resulted in better controlling of the device.
#      
#      \subsection  The Finite State Diagram for the controller task 
# @image html contaskFSM.jpg [This is the finite state machine drawing for the controller task in week 3 of the final project] 
#      \subsection  The Finite State Drawing for the UI task
# @image html uiFSM.jpg [This is the finite state machine drawing for the UI task in week 3 of the final project]
#      \subsection  The Finite State Drawing for the Control task
# @image html taskwk3.jpg [This is task diagram for week 3 of the final project]
#      \subsection  Position Graphs At Various K values 
# @image html graph1.JPG [Graph of the response with the motor targeting 800 RPM and a kp of 0.01]
# @image html graph2.JPG [Graph of the response with the motor targeting 800 RPM and a kp of 0.005]
# @image html graph3.JPG [Graph of the response with the motor targeting 800 RPM and a kp of 0.002]
# @image html graph4.JPG [Graph of the response with the motor targeting 800 RPM and a kp of 0.001]
#      \subsection  L3 Links to Code Referenced
# \ref Bosswk3.py
# \ref ComputerSidewk3.py
# \ref Con_Taskwk3.py
# \ref controllerdriverwk3.py
# \ref encoderdriverwk3.py
# \ref motordriverwk3.py
# \ref shareswk3.py
# \ref UI_Taskwk3.py
#  @}   
#
## \defgroup group8 Final Project Week 4
# 
## \addtogroup group8
# @{   
#      @page FPW4
#      \section sec8 Week 4 of the Final Project 
#      This page contains the subsections
#
#       Week 4 of the final project was the culmination of everything done this
#       quarter. To test the controller as well as the ability of the nucleo to 
#       read a CSV file, a set of test values were presented to the program. 
#       The program then parsed this set of data, and only saved a small number
#       that were both representative of the profile, but could also fit in the 
#       limited RAM of the Nucleo. Not too much code modification was necessay here,
#       as the controller was already set up for a changing input.
#
#       As above, the encoders had issues maininging positional awareness, so it
#       was necessary to use a friend as the hardware interface with their encoders.
#       Since I did not want to bother them a ton over finals week with running 
#       my code, I only had the chance to run it a couple times, so the fine tuning
#       was difficult and led to graphs that were less than optimal. The graph shown
#       below had a J value of about 11,000,000, which is rather dismal. However, 
#       I believe this could have been improved with an integral controller and 
#       a bit more time finetuning the Kp values that I used. A CSV file was also
#       created to document these values. 
#      
#      \subsection  The Finite State Diagram for the controller task 
# @image html contaskwk4.jpg [This is the finite state machine drawing for the controller task in week 4 of the final project] 
#      \subsection  The Finite State Drawing for the UI task
# @image html uiwk4.jpg [This is the finite state machine drawing for the UI task in week 4 of the final project]
#      \subsection  The Finite State Drawing for the Control task
# @image html taskwk4.jpg [This is task diagram for week 4 of the final project]
#      \subsection  Final Graphs of Position and Velocity
# @image html week4.JPG [Graph of the response of the system to a given input]
#      \subsection L4 Links to Code Referenced
# \ref Boss.py
# \ref ComputerSide.py
# \ref Con_Task.py
# \ref controllerdriver.py
# \ref encoderdriver.py
# \ref motordriver.py
# \ref shares.py
# \ref UI_Task.py
#  @}   





