# -*- coding: utf-8 -*-
"""@file lab4terminalsecond.py
@brief This program recieves the output from the nucleo and graphs the results.
@details This program instructs the nucleo when to begin the data collection
         and allows for early termination of the data collection through the 
         use of a user interface. Once the data collection is complete, either 
         through the user stopping the collection or due to the time constraint 
         being satisfied, the data is sent back to this program where it is 
         deconstructed piece by piece into time and output varables. From here,
         the data is plotted.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%201/Completed%20Files/lab4terminalsecond.py
        
@author Matthew Nagy
@date Created on Thu Feb 18 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""
from array import array
import serial
from matplotlib import pyplot
import time
import keyboard
import csv

ser = serial.Serial(port='COM5',baudrate = 115273, timeout = 35)
mytimes = array('f',3001*[0])
myvalues = array('f',3001*[0])
i = 0
done = False 

user_in = input ("Enter 'g' to start data collection from the Nucleo or 's' to stop data collection.")
ser.write(str(user_in).encode())
tstart = time.time()


last_key = None

def kb_cb(key):
    '''    
    @brief   This function handles keyboard callbacks.
    @details This function allows for the keyboard presses to function as 
             callbacks. This utilisises the keyboard for input presses when
             they need to interrupt another function such as data collection.
    '''
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("s", callback=kb_cb)

# Run this loop forever, or at least until someone presses control-C
print('Press s at any point to stop the data collection. It will stop after 30 seconds')
while tstart-time.time() < 30:
    try:
        if last_key is not None:
            break 

    except KeyboardInterrupt:
        break

print(last_key)
if last_key == 's':
    ser.write(str(last_key).encode())
else:
    pass

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()

    

for n in range(301):
    values = ser.readline().decode()
    values = values.strip()
    values = values.split(',')
    print(values)
    
    mytimes[i] = float(values[0])
    myvalues[i] = float(values[1])
    i = i+1

with open('values.csv','w') as file:
    print('making csv')
    for n in range(301):
        writer = csv.writer(file)
        writer.writerow([n,mytimes[n],myvalues[n]])


pyplot.figure()
pyplot.plot(mytimes,myvalues)
pyplot.xlabel('Time')
pyplot.ylabel('Data')
pyplot.title('Data vs Time')



ser.close()
    
            
            





