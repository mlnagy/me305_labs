# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 08:58:36 2021

@author: matth
"""

import utime 
from array import array
import math 
from pyb import UART 
import sys
import pyb
pyb.repl_uart(None)

class lab4:
    
    
    #State Constants 
    S0_INIT = 0
    S1_DataGen = 1
    S2_DataSend = 2
    
    def __init__(self):
        self.myuart = UART(2) #UART is a class not an object, so no need for self.
        self.t0 = 0 # t start, going to need to change this to time 
        self.tend = 30000 # t end
        self.t = 0 # current value of t 
        self.samplerate = 10 # samples per second
        self.tdiff = 1000/self.samplerate
        self.n = 0
        self.times =  array('f',301*[0])
        self.values = array('f',301*[0])
        self.i = 0
        self.usrinput = 0
        self.state = 0
        self.tcurr = 0
    
    
    def run(self):
        if self.state == self.S0_INIT:
            self.t0 = utime.ticks_ms
            self.state = 1
            # print('Transitioning to State 1')
        
        elif self.state == 1:
            if  self.myuart.any() != 0:
                self.newinput = self.myuart.readchar()
                if self.newinput == 103 or self.newinput ==  115:
                    self.usrinput = self.newinput
                    self.tcurr = utime.ticks_ms()
                else:
                    pass
            
            elif self.usrinput == 103:
                if self.t <= self.tend:
                    if utime.ticks_diff(utime.ticks_ms(),self.tcurr) >= self.tdiff:
                        self.values[self.n] = (math.exp(-0.1*(self.t/1000))*math.sin((2/3)*math.pi*(self.t/1000))) 
                        self.times[self.n] = self.t/1000
                        self.t = self.tdiff + self.t
                        self.n = self.n + 1
                        self.tcurr = utime.ticks_ms()
                        print('Doing math')
                    else:
                        pass
                else:
                    self.state = 2
                    print('Transition to S2')
            
            elif self.usrinput == 115:
                self.state = 2
                print('Transition to S2')
                
            else: 
                pass
            
        elif self.state == 2:
            if self.i < len(self.values):
                print('Sending Values')
                self.mystring = ('{:}, {:}\r\n'.format(self.times[self.i], self.values[self.i]))
                self.myuart.write(self.mystring)
                self.i = self.i + 1
                print(self.i)
                print(len(self.mystring))
            else:
                sys.exit()
                
            





