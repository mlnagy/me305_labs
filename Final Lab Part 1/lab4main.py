# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 09:02:27 2021

@author: matth
"""
import pyb 
pyb.repl_uart(None)

from lab4 import lab4


if __name__ == "__main__":
     task1 = lab4()
     while True:
        task1.run()