# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 08:34:15 2021

@author: matth
"""
from array import array
import serial
from matplotlib import pyplot
import time
from threading import Timer 
import os

ser = serial.Serial(port='COM5',baudrate = 115273, timeout = 35)
mytimes = array('f',3001*[0])
myvalues = array('f',3001*[0])
i = 0
done = False 

user_in = input ("Enter 'g' to start data collection from the Nucleo or 's' to stop data collection.")
ser.write(str(user_in).encode())
tstart = time.time()

timeout = 3
t = Timer(timeout,os.system,['clear'])
t.start()
answer = input('Press s to stop data collection.')
ser.write(str('s').encode())
t.cancel()


for n in range(301):
    values = ser.readline().decode()
    values = values.strip()
    values = values.split(',')
    print(values)
    
    mytimes[i] = float(values[0])
    myvalues[i] = float(values[1])
    i = i+1

pyplot.figure()
pyplot.plot(mytimes,myvalues)
pyplot.xlabel('Time')
pyplot.ylabel('Data')

ser.close()
    
            
            





