# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 08:34:15 2021

@author: matth
"""
from array import array
import serial
from matplotlib import pyplot
import time
import keyboard
import csv

ser = serial.Serial(port='COM5',baudrate = 115273, timeout = 35)
mytimes = array('f',3001*[0])
myvalues = array('f',3001*[0])
i = 0
done = False 

user_in = input ("Enter 'g' to start data collection from the Nucleo or 's' to stop data collection.")
ser.write(str(user_in).encode())
tstart = time.time()


last_key = None

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("s", callback=kb_cb)

# Run this loop forever, or at least until someone presses control-C
print('Press s at any point to stop the data collection. It will stop after 30 seconds')
while tstart-time.time() < 30:
    try:
        if last_key is not None:
            break 

    except KeyboardInterrupt:
        break

print(last_key)
if last_key == 's':
    ser.write(str(last_key).encode())
else:
    pass

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()

    

for n in range(301):
    values = ser.readline().decode()
    values = values.strip()
    values = values.split(',')
    print(values)
    
    mytimes[i] = float(values[0])
    myvalues[i] = float(values[1])
    i = i+1

with open('values.csv','w') as file:
    print('making csv')
    for n in range(301):
        writer = csv.writer(file)
        writer.writerow([n,mytimes[n],myvalues[n]])


pyplot.figure()
pyplot.plot(mytimes,myvalues)
pyplot.xlabel('Time')
pyplot.ylabel('Data')



ser.close()
    
            
            





