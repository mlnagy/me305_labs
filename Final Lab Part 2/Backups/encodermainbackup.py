# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 17:32:44 2021

@author: matth
"""

import utime 
from array import array
from pyb import UART, Pin
import pyb
import time

pyb.repl_uart(None)
from encoder1 import Encoder
myencoder = Encoder(Pin.cpu.B6,Pin.cpu.B7)


class tasks:
    
    ## State Constants
    S0_INIT = 0
    S1_DataRec = 1
    S2_DataSend = 2
    
    def __init__(self):
        self.myuart = UART(2) #UART is a class not an object, so no need for self.
        self.state = 0
        self.usrinput = 0
        self.encoderdelta = 0
        self.encoderposition = 0
        self.newinput = 0
        self.times = array('f',1*[0])
        self.values = array('f',1*[0])
        self.short = False
        self.run1 = False
        self.n = 0
        self.i = 0
        
        
        
    def run(self):
         
         myencoder.update()
         if self.state == self.S0_INIT:
            self.t0 = utime.ticks_ms
            self.state = 1
            print('In state 1')
        
         elif self.state == 1:
             if  self.myuart.any() != 0:
                 self.newinput = self.myuart.readchar()
                 if self.newinput == 103 or self.newinput ==  115 or self.newinput ==  100 or self.newinput ==  112 or self.newinput ==  122:
                     self.usrinput = self.newinput
                     self.time = utime.ticks_ms()
                     self.i = 0 # For state 2 uses on second run
                            
             elif self.usrinput == 122: #z
                  myencoder.set_position()
                  pass
             elif self.usrinput == 112: #p
                  self.encoderposition = myencoder.get_position()
                  self.state = 2
                  self.encpos = True
                  self.short = True
                  pass
             elif self.usrinput == 100: #d
                  self.encoderdelta = myencoder.get_delta()
                  self.state = 2
                  self.encdel = True
                  self.short = True 
                  pass
             elif self.usrinput == 103: #g
                  print('In data collection')
                  print(utime.ticks_diff(utime.ticks_ms(), self.time))
                  time.sleep(0.1)
                  if utime.ticks_diff(utime.ticks_ms(), self.time) <= 30000:
                      
                      self.values.append(myencoder.get_position()) # Do we need to append?
                      self.times.append(utime.ticks_diff(utime.ticks_ms(), self.time)) # Append? Array size is unknown rn
                      self.n = self.n + 1
                      print(self.n)
                      
                  elif utime.ticks_diff(utime.ticks_ms(), self.time) > 30000:
                      self.state = 2
             elif self.usrinput == 115: #s
                  self.state = 2
                  pass
              
             else: 
                 pass
         
         elif self.state == 2:
             if self.short == True :
                 if self.run1 == False:
                     print('Sending Info Code')
                     self.mystring = ('{:}, {:}\r\n'.format(2, 2))
                     self.myuart.write(self.mystring)
                     self.run1 = True
                 if self.encpos == True:  
                     print('Sending Encoder Position Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderposition, 0)) # Zero tells interface this is encoder position
                     self.myuart.write(self.mystring)
                     self.short = False
                 if self.encdel == True:
                     print('Sending Encoder Delta Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderdelta, 1)) # One tells interface this is delta 
                     self.myuart.write(self.mystring)
                     self.short = False
             
             
        
             elif self.i < len(self.values):
                if self.run1 == False:
                    print('Sending Info Code')
                    self.mystring = ('{:}, {:}\r\n'.format(len(self.values), len(self.times)))
                    self.myuart.write(self.mystring)
                    self.run1 = True
                else:
                    print('Sending Values')
                    self.mystring = ('{:}, {:}\r\n'.format(self.times[self.i], self.values[self.i]))
                    self.myuart.write(self.mystring)
                    self.i = self.i + 1
                    print(self.i)
                    print(len(self.mystring))
             
             elif self.i >= len(self.values):
                 self.state = 1
                 self.newinput = 0
                 self.usrinput = 0
                 self.run1 = False
                 self.encpos = False
                 self.encdel = False
                 self.times = array('f',1*[0])
                 self.values = array('f',1*[0])
                 
                 
task1 = tasks()

while True:
    
    task1.run()