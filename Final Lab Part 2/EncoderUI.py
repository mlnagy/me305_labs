# -*- coding: utf-8 -*-
"""@file EncoderUI.py
@brief This program recieves the output from the nucleo and graphs the results.
@details This program instructs the nucleo when to begin the data collection
         and allows for early termination of the data collection through the 
         use of a user interface. Once the data collection is complete, either 
         through the user stopping the collection or due to the time constraint 
         being satisfied, the data is sent back to this program where it is 
         deconstructed piece by piece into time and output varables. From here,
         the data is plotted. It allows for multiple user inputs to be selected,
         from starting or stopping data collection, zeroing the encoder, as well
         as getting position and current delta.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%202/EncoderUI.py
        
@author Matthew Nagy
@date Created on Thu Feb 25 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""
from array import array
import serial
from matplotlib import pyplot
import time
import keyboard
import csv

run = True
zpress = False 

while run == True:
    ser = serial.Serial(port='COM5',baudrate = 115273, timeout = 1)
    mytimes = array('f',1*[0])
    myvalues = array('f',1*[0])
    i = 0
    done = False 
    last_key = None
    user_in = input ("Enter 'g' to start data collection from the Nucleo or 's' to stop data collection.")
    ser.write(str(user_in).encode())
    tstart = time.time()
    short = False
    
    
    
    
    def kb_cb(key):
        '''    
        @brief   This function handles keyboard callbacks.
        @details This function allows for the keyboard presses to function as 
             callbacks. This utilisises the keyboard for input presses when
             they need to interrupt another function such as data collection.
        '''
        global last_key
        last_key = key.name
    
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("s", callback=kb_cb)
    keyboard.on_release_key("z", callback=kb_cb)
    keyboard.on_release_key("p", callback=kb_cb)
    keyboard.on_release_key("d", callback=kb_cb)
    keyboard.on_release_key("g", callback=kb_cb)
    
    # Run this loop forever, or at least until someone presses control-C
    
    if user_in == 'g':
        print('Press s at any point to stop the data collection. It will stop after 30 seconds')
        while abs(tstart-time.time())< 30:
            try:
                if last_key is not None:
                    break 
        
            except KeyboardInterrupt:
                break
    
    # print(last_key)
    if last_key == 'z':
        ser.write(str(last_key).encode())
        print('Zeroing Encoder')
    if last_key == 'p':
        ser.write(str(last_key).encode())
        print('Getting Encoder Position')
    if last_key == 'd':
        ser.write(str(last_key).encode())
        print('Getting Encoder Delta')
    if last_key == 's':
        ser.write(str(last_key).encode())
        print('Stopping Data Collection')
    
    
    # Turn off the callbacks so next time we run things behave as expected
    keyboard.unhook_all()
    
    # Recieve first data valuesg
    firstvalues = ser.readline().decode()
    firstvalues = firstvalues.strip()
    firstvalues = firstvalues.split(',')
    print(firstvalues)
    
    if user_in == 'p' or user_in == 'd' or user_in == 'g':
        if int(firstvalues[0]) == 2: # 2 incdicates short transmission
                short = True 
                translength = 1
                datarec = ser.readline().decode()
                datarec = datarec.strip()
                datarec = datarec.split(',')
                if int(datarec[1]) == 0: # If second number is zero, then we have an encoder position
                    encoderposition = datarec[0]
                    print('Encoder position is '+ str(encoderposition))
                elif int(datarec[1]) == 1: # If second number is one, then we have an encoder delta
                    encoderdelta = datarec[0]
                    print('Encoder delta is '+ str(encoderdelta))
        
        
        else:
            translength = int(firstvalues[0])
            # print(translength)
    
    if user_in == 'z':
        zpress = True
    
    if short == False and zpress == False:
        for n in range(translength):
                values = ser.readline().decode()
                values = values.strip()
                values = values.split(',')
                print(values)
                
                mytimes.append(float(values[0]))
                myvalues.append(float(values[1]))
                i = i+1
        
        with open('values.csv','w') as file:
            print('making csv')
            for n in range(translength):
                writer = csv.writer(file)
                writer.writerow([n,mytimes[n],myvalues[n]])
        
        
        pyplot.figure()
        pyplot.plot(mytimes,myvalues)
        pyplot.xlabel('Time (ms)')
        pyplot.ylabel('Position (ticks)')
        pyplot.title('Position vs Time')
    ans = input('To perform another command type yes, otherwise the program will exit:  ')
    if ans == 'yes':
        print('Running Again')
        run = True
    else:
        run = False
        print('Exiting')
    
    ser.close()
    