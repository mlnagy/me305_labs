# -*- coding: utf-8 -*-
"""@file encoder1.py
@brief This class provides the driver support to run the encoders.
@details This program encompasses the class that provides driver support to run
         the encoders. It allows for differnet channels to be used for multiple
         encoders as well as support to get the new position, reset the postion,
         update the locations, as well as get the change in position.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%202/encoder1.py
        
@author Matthew Nagy
@date Created on Thu Feb 25 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 

"""

import pyb
from pyb import Pin, Timer


class Encoder:
    
    
    def __init__(self,CH1,CH2):
        '''    
        @brief   This function initializes the class Enocder
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. This also takes 
                in the paramaters CH1 and CH2.
        @param CH1 provides an input for the first encoder timer channel.
        @param CH2 provides an input for the second encoder timer channel.
        '''
        self.CH1 = CH1
        self.CH2 = CH2
        self.period = 65535
        
        
        self.Timer = Timer(4, period=self.period, prescaler=0)
        self.Timer.channel(1, mode=pyb.Timer.ENC_AB, pin=self.CH1)
        self.Timer.channel(2, mode=pyb.Timer.ENC_AB, pin=self.CH2)
        
        
        self.position = 0
        self.prevnum = 0
        self.currnum = 0
        
        self.delta = 0
       
        
        
    
    def update(self):
        '''    
        @brief   This function updates the location of the encoder.
        @details This function updates the location of the encoder by analyzing
                 the change in position based on both the sign and differnece 
                 between last recorded encoder postions.
        '''
        self.prevnum  = self.currnum
        self.currnum = self.Timer.counter()
        self.delta = self.currnum - self.prevnum
        
        if abs(self.delta) > self.period/2 and self.delta > 0:
            self.delta = self.period - self.delta
        elif abs(self.delta) > self.period/2 and self.delta < 0:
            self.delta = self.period + self.delta
        
        self.position = self.position + self.delta
        
        
            
        
    def get_position(self):
        '''    
        @brief   This function returns the location of the encoder.
        '''
        return self.position 

    
    def set_position(self):
        '''    
        @brief   This function zeros the encoder by setting the current position to zero.
        '''
        self.prevnum = self.Timer.counter()
        self.position = 0
        
    
    def get_delta(self):
        '''    
        @brief   This function returns the change in the encoder postion.
        '''
        return (self.delta)
        
    
    


    