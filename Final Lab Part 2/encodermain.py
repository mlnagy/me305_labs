"""@file encodermain.py
@brief This program runs the encoder and records the output to an array.
@details This program creates the class tasks, which is set up as a finite 
         state machine. This class continualy updates the encoder as well as
         takes input from the PC side. If can recieve a number of commands,
         from zeroing the postion, getting the change in postiion, getting 
         the current postion as well as recording the output for thrity seconds.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%202/encodermain.py
        
@author Matthew Nagy
@date Created on Thu Feb 25 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""

import utime 
from array import array
from pyb import UART, Pin
import pyb


pyb.repl_uart(None)
from encoder1 import Encoder
myencoder = Encoder(Pin.cpu.B6,Pin.cpu.B7)


class tasks:
    
    ## State Constants
    S0_INIT = 0
    S1_DataRec = 1
    S2_DataSend = 2
    
    def __init__(self):
        '''    
        @brief   This function initializes the class tasks
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. This class runs 
                tasks as decribed above as well as recording and sending data PC
                side.
        '''
        self.myuart = UART(2) #UART is a class not an object, so no need for self.
        self.state = 0
        self.usrinput = 0
        self.encoderdelta = 0
        self.encoderposition = 0
        self.newinput = 0
        self.times = array('f',1*[0])
        self.values = array('f',1*[0])
        self.short = False
        self.run1 = False
        self.n = 0
        self.i = 0
        self.currenttime = 0
        self.waittime = 0
        
        
        
    def run(self):
         '''    
        @brief   This function runs the finite state machine of this class.
        @details This function runs the finite state machine of this class. 
                 It contains four states. The first state recieves commands 
                 from the PC and goes to the correct user input. Once this has 
                 been performed, the machine moves to state two where it sends
                 short data, or state three where is sends longer data. Once
                 either of these actions have been completed, the machine 
                 resets itself in state 4, and procedes to state one.    
        '''
         myencoder.update()
         if self.state == self.S0_INIT:
            self.t0 = utime.ticks_ms
            self.state = 1
            print('In state 1')
        
         elif self.state == 1:
             if  self.myuart.any() != 0:
                 self.newinput = self.myuart.readchar()
                 if self.newinput == 103 or self.newinput ==  115 or self.newinput ==  100 or self.newinput ==  112 or self.newinput ==  122:
                     self.usrinput = self.newinput
                     self.time = utime.ticks_ms()
                     self.i = 0 # For state 2 uses on second run
                            
             elif self.usrinput == 122: #z
                  myencoder.set_position()
                  pass
             elif self.usrinput == 112: #p
                  self.encoderposition = myencoder.get_position()
                  self.state = 2
                  self.encpos = True
                  self.short = True
                  pass
             elif self.usrinput == 100: #d
                  self.encoderdelta = myencoder.get_delta()
                  self.state = 2
                  self.encdel = True
                  self.short = True 
                  pass
             elif self.usrinput == 103: #g
                  # print('In data collection')
                  # print(utime.ticks_diff(utime.ticks_ms(), self.time))
                  self.waittime = utime.ticks_diff(utime.ticks_ms(), self.currenttime)
                  # time.sleep(0.1)
                  if utime.ticks_diff(utime.ticks_ms(), self.time) <= 30000 and self.waittime >= 10: # sets time to check each value for sending 
                      print('In data collection')
                      self.values.append(myencoder.get_position()) # Do we need to append?
                      self.times.append(utime.ticks_diff(utime.ticks_ms(), self.time)) # Append? Array size is unknown rn
                      self.n = self.n + 1
                      self.currenttime = utime.ticks_ms()
                      print(self.n)
                      
                  elif utime.ticks_diff(utime.ticks_ms(), self.time) > 30000:
                      self.state = 3
             elif self.usrinput == 115: #s
                  self.state = 3
                  pass
              
             else: 
                 pass
         
         elif self.state == 2:
             if self.short == True :
                 if self.run1 == False:
                     print('Sending Info Code')
                     self.mystring = ('{:}, {:}\r\n'.format(2,2))
                     self.myuart.write(self.mystring)
                     self.run1 = True
                 elif self.encpos == True:  
                     print('Sending Encoder Position Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderposition, 0)) # Zero tells interface this is encoder position
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
                 elif self.encdel == True:
                     print('Sending Encoder Delta Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderdelta, 1)) # One tells interface this is delta 
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
             
             
         elif self.state == 3:
             if self.i < len(self.values):
                if self.run1 == False:
                    print('Sending Info Code')
                    self.mystring = ('{:}, {:}\r\n'.format(len(self.values), len(self.times)))
                    self.myuart.write(self.mystring)
                    self.run1 = True
                else:
                    print('Sending Values')
                    self.mystring = ('{:}, {:}\r\n'.format(self.times[self.i], self.values[self.i]))
                    self.myuart.write(self.mystring)
                    self.i = self.i + 1
                    print(self.i)
                    print(len(self.mystring))
             
             elif self.i >= len(self.values):
                    self.state = 4
                    
         elif self.state == 4 :
                self.state = 1
                self.newinput = 0
                self.usrinput = 0
                self.run1 = False
                self.encpos = False
                self.encdel = False
                self.times = array('f',1*[0])
                self.values = array('f',1*[0])
                 
                 
task1 = tasks()

while True:
    
    task1.run()