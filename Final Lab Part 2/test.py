# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 18:16:33 2021

@author: matth
"""

from pyb import Pin, Timer


TIM4 = Timer(4, period=65535, prescaler=0)
TIM4.channel(1, mode=Timer.ENC_AB, pin=Pin.cpu.B6)
TIM4.channel(2, mode=Timer.ENC_AB, pin=Pin.cpu.B7)

while True:
    print(TIM4.counter())