# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 08:54:48 2021

@author: matth
"""
from pyb import Timer, Pin, delay


TIM3 = Timer(3, freq=20000)
# Motor 1
IN1 = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
IN2 = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
EN = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=1)




IN1.pulse_width_percent(50)
IN2.pulse_width_percent(0)
delay(3000)
IN1.pulse_width_percent(0)
IN2.pulse_width_percent(0)