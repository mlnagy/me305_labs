# -*- coding: utf-8 -*-
"""@file Boss.py
@brief This program runs controller class and the UI class continuously.
@details This program runs the controller class and UI class continuously to 
         provide both a user interface as well as continuous motor control.
         See code here: INSERT LINK HERE
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
@image html PUT FILE HERE This is the finite state machine for the week 3 lab.
@image html PUT FILE HERE This is the task diagram for the week 3 lab.
"""
from Con_Task import ControllerTask
from UI_Task import tasks

task1 = tasks()
cont = ControllerTask()

while True:
    cont.run()
    task1.run()