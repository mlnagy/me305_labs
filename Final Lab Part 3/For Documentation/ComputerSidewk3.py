# -*- coding: utf-8 -*-
"""@file ComputerSidewk3.py
@brief This program sends commands and receives data from the Nucleo.
@details This program handles sending and recieving data from the Nucleo. It 
         can send commands via user inputs. These range from pressing g to begin 
         data collection, d to get current Kp, p to set a new Kp, m to set motor
         RPM, and h to stop the motor. Once the commands have been completed, 
         the user has the option to continue or to exit the program. All of 
         the data is returned to the host PC running this program, and graphs
         the output of the velocity of the motor.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%203/For%20Documentation/ComputerSidewk3.py
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""

from array import array
import serial
from matplotlib import pyplot
import time
import keyboard
import csv

run = True


while run == True:
    ser = serial.Serial(port='COM5',baudrate = 115273, timeout = 1)
    mytimes = array('f',1*[0])
    myvalues = array('f',1*[0])
    i = 0
    done = False 
    last_key = None
    print('Press g to start data collection')
    print('Press d to get current kp')
    print('Press p to set a new kp')
    print('Press m to set a new motor rpm')
    print('Press h to stop the motor')
    user_in = input ()
    ser.write(str(user_in).encode())
    tstart = time.time()
    short = False
    zpress = False 
    
    
    
    
    def kb_cb(key):
        '''    
        @brief   This function handles keyboard callbacks.
        @details This function allows for the keyboard presses to function as 
             callbacks. This utilisises the keyboard for input presses when
             they need to interrupt another function such as data collection.
        '''
        global last_key
        last_key = key.name
    
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("s", callback=kb_cb) #115
    keyboard.on_release_key("m", callback=kb_cb) #109
    keyboard.on_release_key("p", callback=kb_cb) #112
    keyboard.on_release_key("d", callback=kb_cb) #100
    keyboard.on_release_key("g", callback=kb_cb) #103
    keyboard.on_release_key("h", callback=kb_cb) #104
    
    # Run this loop forever, or at least until someone presses control-C
    
    if user_in == 'g':
        print('Press s at any point to stop the data collection. It will stop after 30 seconds')
        while abs(tstart-time.time())< 30:
            try:
                if last_key is not None:
                    break 
        
            except KeyboardInterrupt:
                break
    
  
    if last_key == 'm':
        ser.write(str(last_key).encode())
        print('Preparing to set new RPM')
    
    if last_key == 'p':
        ser.write(str(last_key).encode())
        print('Preparing to Set New Kp')
        
    if last_key == 'd':
        ser.write(str(last_key).encode())
        print('Retrieving Current Kp')
    
    if last_key == 's':
        ser.write(str(last_key).encode())
        print('Stopping Data Collection')
        
    if last_key == 'h':
        ser.write(str(last_key).encode())
        print('Stopping Motor')
    
    
    # Turn off the callbacks so next time we run things behave as expected
    keyboard.unhook_all()
    

    # Recieve first data valuesg
    firstvalues = ser.readline().decode()
    firstvalues = firstvalues.strip()
    firstvalues = firstvalues.split(',')
    print(firstvalues)
    
    if user_in == 'g' or user_in == 'd':
        if int(firstvalues[0]) == 2: # 2 incdicates short transmission
                short = True 
                translength = 1
                datarec = ser.readline().decode()
                datarec = datarec.strip()
                datarec = datarec.split(',')
                if int(datarec[1]) == 0: # If second number is zero, then we have an encoder position
                    currentkp = datarec[0]
                    print('Current Kp is '+ str(currentkp))
                # elif int(datarec[1]) == 1: # If second number is one, then we have an encoder delta
                #     encoderdelta = datarec[0]
                #     print('Encoder delta is '+ str(encoderdelta))
                # old code can be used to recieve another value
        
        
        else:
            translength = int(firstvalues[0])
            print('This is translength',translength)
    
    if user_in == 'p':
        newkp = input('Please Input New Kp')
        ser.write(str(newkp).encode())
        zpress = True
        
    if user_in == 'm':
        newrpm = input('Please Input New Motor Speed')
        ser.write(str(newrpm).encode())
        zpress = True
    
    if short == False and zpress == False:
        print('In Data Recieve Area')
        for n in range(translength):
                values = ser.readline().decode()
                values = values.strip()
                values = values.split(',')
                print(values)
                
                mytimes.append(float(values[0]))
                myvalues.append(float(values[1]))
                n = n + 1
        
        with open('values.csv','w') as file:
            print('making csv')
            for n in range(translength):
                writer = csv.writer(file)
                writer.writerow([n,mytimes[n],myvalues[n]])
        
        
        pyplot.figure()
        pyplot.plot(mytimes,myvalues)
        pyplot.xlabel('Time')
        pyplot.ylabel('Data')
    
    ans = input('To perform another command type yes, otherwise the program will exit:  ')
    if ans == 'yes':
        print('Running Again')
        run = True
    else:
        run = False
        print('Exiting')
    
    ser.close()