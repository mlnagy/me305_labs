# -*- coding: utf-8 -*-
"""@file motordriver.py
@brief This program contains the motor driver class.
@details This program holds the motor driver class. This class recieves a PWM
         value from Con_Task to set the motor speed.
         
         See code here: INSERT LINK HERE
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""
# from pyb import Timer, Pin, delay
# TIM3 = Timer(3, freq = 20000)

class MotorDriver:
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''    
        @brief   This function initializes the class MotorDriver.
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. The pins for 
                the motor as well as the timer are initialized here.
                @param nSLEEP_Pin is the sleep pin.
                @param IN1_pin is the first motor power pin.
                @param IN2_pin is the second motor power pin.
                @param timer is the timer for the motor.
         '''
        self.IN1Pin = IN1_pin
        self.IN2Pin = IN2_pin
        self.nSLEEPpin = nSLEEP_pin
        self.timer = timer
    
    def enable (self):
        '''    
        @brief   This function handles enabling the motor.
        @details This function handles enabling the motor by setting the sleep
                 pin to be high.
        
       '''
        
        self.nSLEEPpin.high()
    
    
    def disable (self):
        '''    
        @brief   This function handles disabling the motor.
        @details This function handles disabling the motor by setting the sleep
                 pin to be low.
        
       '''
        self.nSLEEPpin.low()
    
    
    def set_duty (self, duty):
        '''    
        @brief   This function handles updating the motor logic.
        @details This function handles updating the motor logic by taking
                 the new duty cycle as a PWM from Con_Task and setting it as the 
                 new value. It handles fowards and reverse by decidng which
                 pin to apply the voltage/current to using positive 
                 negative logic.
        @param duty is the PWM level that will be set on the motor.
        
       '''
        self.duty = duty 
        if self.duty > 0 :
            self.IN1Pin.pulse_width_percent(self.duty)
            self.IN2Pin.pulse_width_percent(0)
        elif self.duty < 0 :
            self.IN1Pin.pulse_width_percent(0)
            self.IN2Pin.pulse_width_percent(self.duty)
        elif self.duty == 0 :
            self.IN1Pin.pulse_width_percent(0)
            self.IN2Pin.pulse_width_percent(0)
        
        
        
# if __name__ == '__main__':
    
#     pin_nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
#     pin_IN1    = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
#     pin_IN2    = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
    
    
#     time       = TIM3
    
#     moe        = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, time)
    
#     moe.enable()
    
#     moe.set_duty(50)
    
#     delay (2000)
    
#     moe.set_duty(0)