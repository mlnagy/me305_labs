# -*- coding: utf-8 -*-
"""@file Con_Task.py
@brief This program controlls the different drivers present in the system.
@details This program handles running the various drivers. It reads different 
         variables from shares.py that have been written to by the UI_Task to 
         request different actions. When these variables change, the 
         ControllerTask has the ability to respond. It can do this in a number
         of ways. It reads postional data from the encoder driver which it feeds 
         into the controller driver. This driver returns values for the new PWM
         based on what the current RPM is and what has been requested. From here,
         the class sends this new PWM value into the motor driver to be fed
         into the motor. It also contains a state in the FSM to handle writing 
         positional data to the FSM after a set amount of time.
         
         See code here: INSERT LINK HERE
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
@image html PUT FILE HERE This is the finite state machine diagram for this class.
"""

import utime
from controllerdriver import ClosedLoop
from encoderdriver import Encoder 
from motordriver import MotorDriver
from pyb import Timer, Pin
from pyb import UART, Pin
import pyb
pyb.repl_uart(None)
import shares
from shares import*






class ControllerTask:
    S0_INIT = 0
    S1_Update = 1
    S2_Send = 2

    
    def __init__(self):
        '''    
        @brief   This function initializes the class ControllerTask
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. The pins 
                for the motor, as well as encoders and encoder timers are 
                initialized here.
        '''
        TIM3 = Timer(3, freq = 20000)
        pin_nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP, value=0)
        pin_IN1   = TIM3.channel(1, mode=Timer.PWM, pin=Pin.cpu.B4)
        pin_IN2   = TIM3.channel(2, mode=Timer.PWM, pin=Pin.cpu.B5)
        pin_IN3   = TIM3.channel(3, mode=Timer.PWM, pin=Pin.cpu.B0)
        pin_IN4   = TIM3.channel(4, mode=Timer.PWM, pin=Pin.cpu.B1)
        self.myencoder1= Encoder(Pin.cpu.B6,Pin.cpu.B7,4)
        self.myencoder2 = Encoder(Pin.cpu.C6,Pin.cpu.C7,8)
        self.moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, TIM3)
        self.moe2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, TIM3)
        self.speedreq = shares.speedreq
        self.mycontroller = ClosedLoop()
        self.newkp = shares.newkp
        self.pwm = 0
        self.samplesize = 20 #Sample to Take
        self.tpr = 4000 # Ticks Per Revolution 
        self.state = 0
        self.tdiff = 0
        self.mycontroller.set_Kp(self.newkp)
        self.velocity = 0
        self.tstart = utime.ticks_ms()
        self.reset = False
        self.run1 = True 
        self.ultstart = 0
        self.position = 0
        
    def run(self): # Runs Controller Task, will be called from boss.py
        '''    
        @brief   This function runs the ControllerTask.
        @details This function runs the controller task for the class. It is set
                 up as a FSM with 5 states. The 0 state initializes the motors
                 depending on shares conditions. State one gets the delta 
                 position from the encoder and calculates the RPM based on 
                 the change in position and change in time. Next, state 2 
                 sends this data to the controller which provides a modified
                 PWM to adjust the speed of the motor based on current speed 
                 and required speed.
        '''
        if self.state == 0:
            if shares.motenable == True:
                self.moe2.enable()
            elif shares.motenable == False:
                self.moe2.disable()
            self.state = 1
    
        if self.state == 1 : # Get Velocity RPM
            self.starttime = utime.ticks_us()
            self.myencoder2.update()
            self.position = self.myencoder2.get_position()
            self.tdiff = utime.ticks_diff(utime.ticks_us(),self.starttime)
            self.delta = self.myencoder2.get_delta()
           
            if self.tdiff > 0:
                self.velocity = ((self.delta/self.tpr)/(self.tdiff))*6000000 # RPM
            elif self.tdiff == 0:
                print('Sample too fast')
            self.state = 2
           
        
        elif self.state == 2: # Updates From Controller
            self.speedreq = shares.speedreq
            self.pwm = self.mycontroller.update(self.velocity,self.speedreq)
            shares.currentkp = self.mycontroller.get_Kp()
            self.mycontroller.set_Kp(shares.newkp)
            self.state = 3
            
        
        elif self.state == 3 : # Updates Motor Duty Cycle
            self.moe2.set_duty(self.pwm)
            if shares.record == False:
                self.state = 0
            if self.reset == True:
                self.tstart = utime.ticks_ms()
                self.reset = False 
                self.stae = 4
            if shares.record == True:
                self.state = 4
            if shares.record == True and self.run1 == True:
                self.ultstart = utime.ticks_ms()
                
        elif self.state == 4: # Writes positional data to shares iterating over a set time 
        
            if abs(utime.ticks_diff(self.tstart, utime.ticks_ms())) > 100:
                # print('Appending Data')
                shares.times.append(abs((utime.ticks_diff(self.ultstart, utime.ticks_ms()))))
                shares.velocity.append(self.velocity)
                shares.position.append(self.position)
                self.tstart = utime.ticks_ms()
                self.reset = True 
                self.run1 = False
            if utime.ticks_diff(self.ultstart, utime.ticks_ms()) > 30000:
                self.run1 = True 
            # Once the time is complete change shares.done to true and shares.record to false
            self.state = 0

    
