# -*- coding: utf-8 -*-
"""@file UI_Task.py
@brief This program interfaces with the PC program and sets tasks.
@details This program creates the class tasks, which is set up as a finite 
         state machine. This class takes input from the user PC side program
         and sets what should happen with the Con_Task. It can take a number 
         of inputs from setting a Kp, reading a Kp, setting a goal RPM, and 
         stopping the motor. It can send the data back to the user by creating 
         a CSV list to be sent over serial communications.
         See code here: INSERT LINK HERE
        
@author Matthew Nagy
@date Created on Thu Feb 25 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
@image html PUT FILE HERE This is the finite state machine for the game.

"""

import utime 
from array import array
from pyb import UART, Pin
import pyb
import shares
from shares import *

pyb.repl_uart(None)



class tasks:
    
    ## State Constants
    S0_INIT = 0
    S1_DataRec = 1
    S2_DataSend = 2
    
    def __init__(self):
        '''    
        @brief   This function initializes the class tasks
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. This class runs 
                tasks as decribed above as well as recording and sending data PC
                side.
        '''
        self.myuart = UART(2) #UART is a class not an object, so no need for self.
        self.state = 0
        self.usrinput = 0
        self.newinput = 0
        self.times = array('f',1*[0])
        self.values = array('f',1*[0])
        self.short = False
        self.run1 = False
        self.n = 0
        self.i = 1
        self.currenttime = 0
        self.waittime = 0
        self.newk = False
        self.newkp = 0
        self.currentkp = shares.currentkp
        self.sendkp = False
        self.newrpm = False
        self.newspeed = 0
        self.x = 0
        self.newtime = array('f',1*[0])
        self.newvelocity = array('f',1*[0])
        self.newposition = array('f',1*[0])
        self.uptime = 0
        self.position = 0
        
        
    def run(self):
         '''    
        @brief   This function runs the finite state machine of this class.
        @details This function runs the finite state machine of this class. 
                 It contains four states. The first state recieves commands 
                 from the PC and goes to the correct user input. Once this has 
                 been performed, the machine moves to state two where it sends
                 short data, or state three where is sends longer data. Once
                 either of these actions have been completed, the machine 
                 resets itself in state 4, and procedes to state one.    
        '''
         if self.state == self.S0_INIT:
            self.t0 = utime.ticks_ms # Not needed 
            self.state = 1
            n = 0
            with open ('reference.csv','r') as file:
                for line in file:
                    line_Str = file.readline()
                    line_Str = line_Str.strip()
                    data = line_Str.split(',')
            
                    try:
                        if n == 10:
                            self.newtime.append(float(data[0]))
                            self.newvelocity.append(float(data[1]))
                            self.newposition.append(float(data[2]))
                            n = 0
                        n += 1
                    except:
                        pass 
        
         elif self.state == 1:
             if  self.myuart.any() != 0:
                 self.newinput = self.myuart.readchar()
                 if self.newinput == 115 or self.newinput ==  109 or self.newinput ==  112 or self.newinput ==  100 or self.newinput ==  103 or self.newinput ==  104:
                     self.usrinput = self.newinput
                     self.time = utime.ticks_ms()
                     self.i = 0 # For state 2 uses on second run
                 
                 if self.newk == True: # sets up recieve for the new kp
                     self.newkp = self.myuart.readline()
                     self.newkp = self.newkp.strip()
                     self.newkp = float(self.newkp)
                     shares.newkp = self.newkp
                     self.newk = False
                     self.usrinput = 0
                     
                 if self.newrpm == True:
                     self.newspeed = self.myuart.readline()
                     self.newspeed = self.newspeed.strip()
                     self.newspeed = float(self.newspeed)
                     shares.speedreq = self.newspeed
                     shares.motenable = True 
                     self.newrpm = False
                     self.usrinput = 0
                     
            
             elif self.usrinput == 112: #p New Kp Coming  
                  self.newk = True
                  
                  
             elif self.usrinput == 100: #d Print out new Kp
                  self.currentkp = shares.currentkp
                  self.state = 2
                  self.sendkp = True
                  self.short = True 
             
             
             elif self.usrinput == 109: #m Set New Motor RPM
                  self.newrpm = True 
             
    
             elif self.usrinput == 103: #g waits until shares sends complete then wiretes new data to array 
                  if utime.ticks_diff(utime.ticks_ms(), self.time) <= 14900: # sets time to check each value for sending #Editing Here
                      shares.record = True 
                      shares.motenable = True
                      shares.speedreq = self.newvelocity[self.x]
                      self.uptime = self.newtime[self.x]
                      print(self.uptime)
                      if utime.ticks_diff(utime.ticks_ms(), self.time)/1000 >= self.uptime:
                          self.x += 1
                      
                  elif utime.ticks_diff(utime.ticks_ms(), self.time) > 14900:
                      self.times = shares.times
                      self.velocity = shares.velocity
                      shares.record = False
                      shares.motenable = False 
                      self.state = 3
             
             elif self.usrinput == 115: #s Stop Recording 
                  self.times = shares.times
                  self.velocity = shares.velocity
                  self.position = shares.position
                  shares.motenable = False 
                  self.state = 3
                  
              
             elif self.usrinput == 104: # h Stop Motor 
                 shares.motenable = False 
                 pass
         
         elif self.state == 2:
             if self.short == True :
                 if self.run1 == False:
                     print('Sending Info Code')
                     self.mystring = ('{:}, {:}\r\n'.format(2,2))
                     self.myuart.write(self.mystring)
                     self.run1 = True
                 elif self.sendkp == True:  
                     print('Sending Current Kp Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.currentkp, 0)) # Zero tells interface this is encoder position
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
                 elif self.encdel == True:
                     print('Sending Encoder Delta Short Info')
                     self.mystring = ('{:}, {:}\r\n'.format(self.encoderdelta, 1)) # One tells interface this is delta 
                     self.myuart.write(self.mystring)
                     self.short = False
                     self.state = 4
             
             
         elif self.state == 3:
             if self.i < len(self.velocity):
                if self.run1 == False:
                    print('Sending Info Code')
                    self.mystring = ('{:}, {:}\r\n'.format(len(self.velocity), len(self.times)))
                    self.myuart.write(self.mystring)
                    self.run1 = True
                else:
                    print('Sending Values')
                    self.mystring = ('{:}, {:}, {:}\r\n'.format(self.times[self.i]/1000, self.velocity[self.i], shares.position[self.i]))
                    self.myuart.write(self.mystring)
                    self.i = self.i + 1
                    print(self.i)
                    print('This is length of the data to be sent on nucleo',len(self.velocity))
             
             elif self.i >= len(self.velocity):
                    print('Moving to state 4')
                    self.state = 4
                    
         elif self.state == 4 :
                self.state = 1
                self.newinput = 0
                self.usrinput = 0
                self.run1 = False
                self.currentkp = False
                self.encdel = False
                self.times = array('f',1*[0])
                self.velocity = array('f',1*[0])
                shares.motenable = False 
