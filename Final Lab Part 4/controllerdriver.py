# -*- coding: utf-8 -*-
"""@file controllerdriver.py
@brief This program contains the controller driver class.
@details This program holds the controller driver class. This class recieves
         data from the Con_Task and responds with a PWM to be sent to the motor
         driver.
         
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%204/controllerdriver.py
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""


class ClosedLoop:
    '''    
        @brief   This function initializes the class ClosedLoop
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. Variables 
                such as maximum and minimum allowed power are laocted here.
    '''
    def __init__(self):
        self.pmax = 99.5 # Max PWM
        self.pmin = -99.5 # Min PWM 
        self.pwm = 30 # Current PWM
        self.kp = 0 # Gain Constant
        self.deltapwm = 0 # Last Change in PWM
        self.newpwm = 30
       
    
    def update(self,speedcurrent,speedrequest):
        '''    
        @brief   This function handles updating the controller logic.
        @details This function handles updating the controller logic by taking
                 the information sent by the Con_Task in the the form of 
                 speedcurrent and speedrequest. It decides the change in PWM
                 based on the difference between these two values, with Kp 
                 deciding how big of a change that is given.
        @param speedcurrent is the current speed the motor is running.
        @param speedrequest is the speed requested for the motor.
       '''
        self.speedreq = speedrequest ## Request this from controller task 
        self.speedcurrent = speedcurrent
        self.deltapwm = self.kp*(self.speedreq - self.speedcurrent)
        self.newpwm = self.newpwm + self.deltapwm
        # print('This is newpwm',self.newpwm)
        if self.newpwm >= self.pmin and self.newpwm <= self.pmax:
            self.pwm = self.pwm + self.deltapwm
            # print('Changing Speed')
            # print('This is delta pwm', self.deltapwm)
            
            
            return self.pwm
        else:
            self.pwm = self.pmax
            # print('Max Or Minimum Power Achieved')
            self.newpwm = self.pmax
            return self.pwm
    
            
       
    
    def get_Kp(self): # Sends out KP to place calling it
        '''    
        @brief   This function gives out Kp to the line calling it.
        '''
        return self.kp
    
    def set_Kp(self, newkp): # Sets kp to current kp from controller task
        '''    
        @brief   This function handles setting a new Kp value.
        @details This function handles setting a new Kp value from shares that
                 is defined by the UI_Task and provided by the user.
        @param newkp is the newkp that should be set.
        '''
        self.kp = newkp
      