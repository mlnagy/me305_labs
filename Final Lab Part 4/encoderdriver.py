# -*- coding: utf-8 -*-
"""@file encoderdriver.py
@brief This program contains the encoder driver class.
@details This program holds the encoder driver class. This class recieves 
         update commands and responds with either position, change in position,
         or setting a new position.
         
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%204/encoderdriver.py
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""

import pyb
from pyb import Timer


class Encoder:
   
    
    def __init__(self,CH1,CH2,timer): # Need to add timer for second motor.
        '''    
        @brief   This function initializes the class Encoder.
        @details Variables required are set in this stage, and any variable that 
                needs to be kept through runs is initialized here. The channels
                for the encoders as well as the timers are passed into this 
                function.
                @param CH1 is the first encoder channel.
                @param CH2 is the second encoder channel.
                @param timer is the timer channel.
         '''
        self.CH1 = CH1
        self.CH2 = CH2
        self.period = 65535
        self.timer = timer
        
        self.Timer = Timer(self.timer, period=self.period, prescaler=0)
        self.Timer.channel(1, mode=pyb.Timer.ENC_AB, pin=self.CH1)
        self.Timer.channel(2, mode=pyb.Timer.ENC_AB, pin=self.CH2)
        
        
        self.position = 0
        self.prevnum = 0
        self.currnum = 0
        
        self.delta = 0
       
        
        
    
    def update(self):
        '''    
        @brief   This function handles updating the encoder logic.
        @details This function handles updating the encoder logic by taking
                 the diffeence between the two recorded postions and decies
                 weather to add or subtract it from the previous based on 
                 signage.
        
       '''
        self.prevnum  = self.currnum
        self.currnum = self.Timer.counter()
        self.delta = self.currnum - self.prevnum
        
        if abs(self.delta) > self.period/2 and self.delta > 0:
            self.delta = self.period - self.delta
        elif abs(self.delta) > self.period/2 and self.delta < 0:
            self.delta = self.period + self.delta
        
        self.position = self.position + self.delta
        
        
            
        
    def get_position(self):
        '''    
        @brief   This function returns the location of the encoder.
        '''
        return self.position 

    
    def set_position(self):
        '''    
        @brief   This function zeros the encoder by setting the current position to zero.
        '''
        self.prevnum = self.Timer.counter()
        self.position = 0
        
    
    def get_delta(self):
        '''    
        @brief   This function returns the change in the encoder postion.
        '''
        return (self.delta)
        
    
    


    