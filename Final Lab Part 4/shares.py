# -*- coding: utf-8 -*-
"""@file shares.py
@brief This program contains variables to be used throught the system.
@details This program contains variables to be used throughout the system. By
         placing them in shares, the variables are accesable to all files by 
         importing. 
         
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Final%20Lab%20Part%204/shares.py
        
@author Matthew Nagy
@date Created on Thu Mar 4 09:40:55 2021
@copyright Matthew Nagy 2021, Inspired by lecture and code provided by Charlie Revfem 
"""

from array import array 

times = array('f',1*[0]) #Times at which the position was recorded, subtract out first value from all values)
velocity = array('f',1*[0])
position = array('f',1*[0]) # RPM at the various times
start = False # Indicates if motors should start
record = False  # Indicates if time and velocity should be recorded to shares files
done = False # Indicates if the 30 seconds are up
newkp = 0.001
speedreq = 800
currentkp = 0.001
motenable = False
reccorddone = False 
