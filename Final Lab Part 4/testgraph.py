# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 00:57:36 2021

@author: matth
"""

from array import array
from matplotlib import pyplot 

genpos = [0,1,2,3,4,5,6,7,8]
time0 = [0,1,2,3,4,5,6,7,8]
providedpos = [1,3,5,7,9,11]
providedtime0 = [0,1,2,3,4,5]

genvel = [0,1,2,3,4,5,6,7,8]
time1 = [0,1,2,3,4,5,6,7,8]
providedvel = [1,3,5,7,9,11]
providedtime1 = [0,1,2,3,4,5]

fig, (ax1,ax2) = pyplot.subplots(2)
fig.suptitle('The good good')
ax1.plot(time0,genpos,label = 'Generated Position')
ax1.plot(providedtime0,providedpos,label = 'Provided Position')
ax1.legend()
ax1.set_xlabel('Time')
ax1.set_ylabel('Position')
ax2.plot(time1,genvel,label = 'Generated Velocity')
ax2.plot(providedtime1,providedvel,label = 'Provided Velocity')
ax2.legend()
ax2.set_xlabel('Time')
ax2.set_ylabel('Velocity')