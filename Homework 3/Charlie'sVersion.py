import time
import random
# Function Definitions go here
def motor_cmd(cmd):
    if  cmd=='FWD':
        print('Mot forward')
    elif cmd=='REV':
        print ('Mot reverse')
    elif cmd == 'STOP':
        print ('Mot Stop')

def L_sensor():
    return random.choice([True, False])
def R_sensor():
    return random.choice([True, False])
def go_button():
    return random.choice([True, False])


# Main Program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state 
    
    while True:
        try:
            #main program code goes here
            print ('During')
            if state==0:
                # run state 0 (init) code code
                print('S0')
                motor_cmd('FWD')
                state = 1
            
            
            elif state ==1:
                print('S1')
                # run state 1 (moving left) code
                if L_sensor():
                    motor_cmd('STOP')
                    state = 2
                    
            
            elif state ==2:
                print('S2')
                # run state 2 (stopped at left) code
                if go_button():
                    motor_cmd('REV')
                    state = 3
            
            
            elif state ==3:
                print('S3')
                # run state 3 (moving right) code
                if R_sensor():
                    motor_cmd('STOP')
                    state = 4
            
            
            elif state ==4:
                print('S4')
                # run state 4 (stopped at right) code
                motor_cmd('FWD')
                state = 1
                
            else: 
                pass
                #code to run if state number is invalud
                #program should ideally never reach here
            
            
            # Slow down execution of FSM so we can see output in consol
            time.sleep(0.2)
           
        except KeyboardInterrupt: 
                # This except block catehes "Ctrl-C" from the keyboard to end the 
                # while(True) loop when desired
                break
            
    # Program de-initialization goes here
    print ('After')