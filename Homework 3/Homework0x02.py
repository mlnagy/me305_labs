'''@file Homework0x02.py
@brief          Simulates a two floor elevator.
@details        Implements a finite state machine, shown below, to simulate
                the behaivor of a two floor elevator.
                Download the code at https://bitbucket.org/mlnagy/me305_labs/src/master/Homework%203/HW3.py
               
@author         Matthew Nagy
@date           1/19/21
@copyright      Licence Info Here

'''

import time
import random
# Function Definitions go here
def motor_cmd(cmd):
    '''@brief Commands the motor to move up, down, or stop
       @param cmd The command to give the motor.
    '''
    if  cmd=='UP':
        print('Elevator Motor Moving Up')
    elif cmd=='DOWN':
        print ('Elevator Motor Moving Down')
    elif cmd == 'STOP':
        print ('Elevator Motor Stopped')

def button_1():
    '''@brief Simulates a random press of button 1 for floor 1.
    '''
    return random.choice([True, False])
def button_2():
    '''@brief Simulates a random press of button 2 for floor 2.
    '''
    return random.choice([True, False])
# def go_button():
#     return random.choice([True, False])


# Main Program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state 
    print ('Elevator Start')
    while True:
        try:
            #main program code goes here
            
            if state==0:
                # run state 0 (init) code code
                print('First Floor Requested')
                motor_cmd('DOWN')
                state = 1
            
            
            elif state ==1:
                print('Stopped At First Floor')
                # run state 1 (moving left) code
                motor_cmd('STOP')
                if button_2():
                    state = 2
                elif button_2():
                    print('Already at First Floor')
                    
            
            elif state ==2:
                print('Second Floor Requested')
                # run state 2 (stopped at left) code
                motor_cmd('UP')
                state = 3
            
            
            elif state ==3:
                print('Stopped At Second Floor')
                # run state 3 (moving right) code
                motor_cmd('STOP')
                if button_1():
                    state = 0
                elif button_2():
                    print('Already At Second Floor')
                    

            
            # elif state ==4:
            #     print('S4')
            #     # run state 4 (stopped at right) code
            #     motor_cmd('FWD')
            #     state = 1
                
            else: 
                pass
                #code to run if state number is invalud
                #program should ideally never reach here
            
            
            # Slow down execution of FSM so we can see output in consol
            time.sleep(1)
           
        except KeyboardInterrupt: 
                # This except block catehes "Ctrl-C" from the keyboard to end the 
                # while(True) loop when desired
                break
            
    # Program de-initialization goes here
    print ('Day Over')