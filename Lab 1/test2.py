
def fib (idx):  # Defines the function for Fibonacci Calculator
    i = 2       # Sets initial index to 2 
    f0 = 0      # Sets the first initial condition
    f1 = 1      # Sets the second initial condition
    while i <= idx: # Sets the conditions for the while loop to run
        fi = f0 + f1 # Adds the first two terms of the sequence and sets as fi
        f0 = f1      # Sets f1 to be f0
        f1 = fi      # Sets f1 to be fi to prep for the next loop
        i = i+1      # Adds the next number to the index
    return f1        # Returns the number from the function


if __name__ == '__main__':  # Sets the function to be called as test with user prompt
    answer = 'yes'          # Sets primary stop condition as yes
    while answer == 'yes':  # Starts while loop with conditons from earlier
        while True:         # Sets loop to run until broken
            try:            # Initializes the try command
                usrinput = input('Please enter the index to calculate the Fibonnaci sequence at: ') # Takes user input as sets as variable
                int(usrinput)   # Ensures the user input variable was an interget
                if int(usrinput)<0: # Calculates if the user input is less tha zero
                    raise ValueError # If the user input was less than zero, the programs enters an error
            except: # Sets except 
               print('Please enter a positive interger: ') # Gives a response if an error occurs
            else:   # Sets else statement 
                if int(usrinput) == 0: # Checks if user input was zero
                    output = 0 # Outputs zero as answer
                    break # Breaks loops
                else:# Sets else
                    output = fib(int(usrinput)) # Sets the sequence calculator to run
                break # Brekas the loop
        print ('Fibonacci number at ''index {:} is {:}.'. format(usrinput,output)) # Formats and prints the sequence output
        answer = input('Would you like to run another number? Type yes or no: ') # Asks if the user would like another input and sets the answer to yes or no
      
