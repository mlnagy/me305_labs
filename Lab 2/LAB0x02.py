# -*- coding: utf-8 -*-
"""@file LAB0x02.py
@brief This program takes a button press and displays various LED sequences.
@details This program prompts the user for a button press to display a light show.
         When the user presses the button, the program displays the first light show 
         on repeat until the button is pressed again. It will go through three 
         seperate configurations with the first being a blinking light, the 
         second being a sine wave display and the third being a "sawtooth" 
         display.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Lab%202/LAB0x02.py
         See video of it functioning here: https://photos.app.goo.gl/R3TGEgkoNtCbb5ZS9
@author Matthew Nagy
@date Created on Thu Jan 21 09:40:55 2021
@copyright Licence Info
@image html Lab2LightsStateDiagram.jpg This is the finite state machine for the LED light show
"""

import pyb
import utime
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)
import math

state = 0
start = 0
print('Please press button B1 (the blue button) to begin the light show, and press the button at any time to see the next light show')
button_push = False
def counter(pin):
       '''    
       @brief   This function handles the button press and sets button push.
       @details This function is what is called when the button is pressed. It 
                takes care of setting the button push variable to set the FSM
                to move to the next state.
       '''
       global button_push
       button_push = True
         
ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=counter)

while True:

    tim2 = pyb.Timer(2, freq = 20000)
    LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
    time = 0
    if  state == 0:
        if button_push == True:
           print('Displaying "Blink" light show')
           state = 1
           button_push = False
    
    if  state == 1 :
        if utime.ticks_diff(utime.ticks_ms(),start) < 500:
            LEDINT.pulse_width_percent(100)
        if utime.ticks_diff(utime.ticks_ms(),start) >= 500 and utime.ticks_diff(utime.ticks_ms(),start) <= 1000:
            LEDINT.pulse_width_percent(0)
        if utime.ticks_diff(utime.ticks_ms(),start)  >1000:
            start = utime.ticks_ms()
        if button_push == True:
               print('Displaying "Sinewave" light show')
               state = 2
               button_push = False
               time = 0

    elif state == 2:
        time = utime.ticks_diff(utime.ticks_ms(),start)/1000
        x = (time % 100)
        wave = 50*math.sin((6*math.pi/20)*x)+50
        LEDINT.pulse_width_percent(wave)
        if button_push == True:
               print('Displaying "Sawtooth" light show')
               state = 3
               button_push = False
               time = 0
    
    elif state == 3:
        time = utime.ticks_diff(utime.ticks_ms(),start)/10
        duty = (time % 100)
        LEDINT.pulse_width_percent(duty)
        if button_push == True:
               print('Displaying "Blink" light show')
               state = 1
               button_push = False
               time = 0
    else:
        pass