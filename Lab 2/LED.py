# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 09:40:55 2021

@author: matth
"""
import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5) # Imports the LED for use
Button = pyb.Pin (pyb.Pin.cpu.C13) # Imports the button for use 
import math 



print('Hello')


def onButtonPressFCN(IRQ_src): #Handles Button Press
    pass 

def LEDStyle(num):
    tim2 = pyb.Timer(2, freq = 20000)
    LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
    time = 0
    interrupt = 0
    if  num == 1 :
        while interrupt==0:
            LED.high()
            pyb.delay(1000)
            LED.low()
            pyb.delay(1000)
            if ButtonInt:
                interrupt = 1
    elif num == 2:
        x = 0
        while interrupt==0:
             wave = 50*math.sin((6*math.pi/20)*x)+50
             LEDINT.pulse_width_percent(wave)
             x = x+0.05
             pyb.delay(50)
             if ButtonInt:
                interrupt = 1
    
    elif num == 3:
        time = 0
        while interrupt==0:
             if time == 1.05:
                time = 0
             else:
                LEDINT.pulse_width_percent(time)
                time = time+0.05
                pyb.delay(50)
                if ButtonInt:
                   interrupt = 1

ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback = onButtonPressFCN)

if __name__ == "__main__":
    graph = 0
    while True:
        if graph <=2:
            if ButtonInt:
               add = 1
            else:
               add = 0
            graph = 1 + add
        
        elif graph >=3:
             graph = 1
        LEDStyle(graph)
    
    


