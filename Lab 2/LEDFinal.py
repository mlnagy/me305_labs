# -*- coding: utf-8 -*-
"""@file LEDFinal.py
@brief This program takes a button press and displays various LED sequences.
@details This program prompts the user for a button press to display a light show.
         When the user presses the button, the program displays the first light show 
         on repeat until the button is pressed again. It will go through three 
         seperate configurations with the first being a blinking light, the 
         second being a sine wave display and the third being a "sawtooth" 
         display.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Lab%202/LEDFinal.py
@author Matthew Nagy
Created on Thu Jan 21 09:40:55 2021
@copyright Licence Info
@image html Lab2LightsStateDiagram.jpg This is the finite state machine for the LED light show
"""

import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)
import math 


num1 = 0
print('Please press button B1 (the blue button) to begin the light show, and press the button at any time to see the next light show')

def counter(pin):
       '''    
       @brief   This function handles the button press and handles counting presses.
       @details This function is what is called when the button is pressed. It 
                takes care of resetting the press count to ensure the program 
                is always running in the 1st, 2nd, or third light show.
       '''
       global num1
       if num1 <=2:
           num1 = num1 + 1
       else:
               num1 = 1
               


def LEDStyle():
    '''    
       @brief   This function handles the different light shows and their equations.
       @details This function handles the different light shows as well as intializes
                the timer chancles and led PWM code. It also handles the if conditions
                for the different light shows as well as the equations used to 
                properly display the different LED brightnesses and spacing.
    '''
    global num1
    tim2 = pyb.Timer(2, freq = 20000)
    LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
    time = 0
    interrupt = 0
    
    if  num1 == 1 :
        print('Displaying "Blink" light show')
        while interrupt==0:
            LEDINT.pulse_width_percent(100)
            pyb.delay(500)
            LEDINT.pulse_width_percent(0)
            pyb.delay(500)
            if num1 != 1:
                interrupt = 1
    
    if num1 == 2:
        interrupt = 0
        x = 0
        print('Displaying "Sinewave" light show')
        while interrupt==0:
              wave = 50*math.sin((6*math.pi/20)*x)+50
              LEDINT.pulse_width_percent(wave)
              x = x+(1/30)
              pyb.delay(50)
              if num1 != 2:
                interrupt = 1
    
    if num1 == 3:
        interrupt = 0
        time = 0
        print('Displaying "Sawtooth" light show')
        while interrupt==0:
              if time >= 100:
                time = 0
              if time < 100:
                LEDINT.pulse_width_percent(time)
                time = time+5
                pyb.delay(50)
                if num1 != 3:
                    interrupt = 1

ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=counter)

while True:
    LEDStyle()



    


