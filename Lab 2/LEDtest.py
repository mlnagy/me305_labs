# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 09:40:55 2021

@author: matth
"""

import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)
import math 


num1 = 0
print('Please press the button to begin the light show')

def my_func(pin):
       '''    
       @brief   This function handles the button press and handles counting presses.
       @param pin This is a filler value that ensur python does not give an error.
       @return The function doesn't return a value, but modifies global num variable.
       '''
       global num1
       if num1 <=2:
           num1 = num1 + 1
       else:
               num1 = 1


def LEDStyle():
    global num1
    tim2 = pyb.Timer(2, freq = 20000)
    LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
    time = 0
    interrupt = 0
    pyb.delay(1000)
    
    if  num1 == 1 :
        print('Displaying Light Series 1')
        while interrupt==0:
            LEDINT.pulse_width_percent(0)
            pyb.delay(1000)
            LEDINT.pulse_width_percent(100)
            pyb.delay(1000)
            if num1 != 1:
                interrupt = 1
    
    if num1 == 2:
        x = 0
        print('Displaying Light Series 2')
        while interrupt==0:
              wave = 50*math.sin((6*math.pi/20)*x)+50
              LEDINT.pulse_width_percent(wave)
              x = x+(1/30)
              pyb.delay(50)
              if num1 != 2:
                interrupt = 1
    
    if num1 == 3:
        time = 0
        print('Displaying Light Series 3')
        while interrupt==0:
              if time >= 100:
                time = 0
              if time < 100:
                LEDINT.pulse_width_percent(time)
                time = time+5
                pyb.delay(50)
                if num1 != 3:
                    interrupt = 1

ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=my_func)

while True:
    LEDStyle()



    


