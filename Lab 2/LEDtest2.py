# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 09:40:55 2021

@author: matth
"""

import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)




num1 = 0

def boop_counter():
    global num1
    num1 = num1 + 1
    print(num1)
    return num1



def my_func(pin):
    pyb.delay(100)  
    boop_counter()


Button.irq(my_func)