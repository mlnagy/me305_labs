# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 09:40:55 2021

@author: matth
"""

import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)
import math 



num1 = 0

def boop_counter():
    global num1
    num1 = num1 + 1
    print(num1)



def my_func(pin):
       boop_counter()


def LEDStyle():
    global num1
    tim2 = pyb.Timer(2, freq = 20000)
    LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
    time = 0
    interrupt = 0
    print("Yay!!!!!!")
    pyb.delay(1000)
    # boop_counter()
    if  num1 == 4 :
        while interrupt==0:
            print('In loop 1')
            LED.high()
            pyb.delay(1000)
            LED.low()
            pyb.delay(1000)
            if num1 != 4:
                interrupt = 1
                print('Interrupted')
    if num1 == 5:
        x = 0
        while interrupt==0:
              print('In Loop 2')
              wave = 50*math.sin((6*math.pi/20)*x)+50
              LEDINT.pulse_width_percent(wave)
              x = x+0.05
              pyb.delay(50)
              if num1 != 5:
                interrupt = 1
    
    if num1 == 6:
        time = 0
        while interrupt==0:
              print('In Loop 3')
              if time == 1.05:
                time = 0
              else:
                LEDINT.pulse_width_percent(time)
                time = time+0.05
                pyb.delay(50)
                if num1 != 6:
                    interrupt = 1

Button.irq(my_func)

while True:
    LEDStyle()


#print('Hello')

# def onButtonPressFCN(IRQ_src): #Handles Button Press
#     if Button() == 0:    
#         print('Nice')
#     else:
#         print('Sad')


# while True :
#     LEDStyle()



# if __name__ == "__main__":
#     graph = 0
#     while True:
#         if graph <=2:
#             if ButtonInt:
#                 add = 1
#             else:
#                 add = 0
#             graph = 1 + add
        
#         elif graph >=3:
#               graph = 1
#         LEDStyle(graph)
    
    


