# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 22:01:03 2021

@author: matth
"""
import pyb
LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)

num1 = 0

def boop_counter():
    global num1
    num1 = num1 + 1
    print(num1)
    return num1


def my_func(pin):
    if Button() == 0:
       LED.on()
       boop_counter()
    else:
        LED.off()

Button.irq(my_func)