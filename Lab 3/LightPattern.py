# -*- coding: utf-8 -*-
"""@file LightPattern.py
@brief This program creates a light sequence for the user to replicate.
@details This program creates a simon says game. The program displays a series of 
         five lights and has the user replicate the pattern. If they get it correct
         they move on to the next stage. If they don't they have the option to restart
         or to quit. It displays a win loss ratio upon winning or loosing.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Lab%203/LightPattern.py
         See video of it functioning here: https://photos.app.goo.gl/Tenf8T2HNxmstbGb6
@author Matthew Nagy
@date Created on Thu Feb 4 09:40:55 2021
@copyright Licence Info
@image html SimonSaysFSM.jpg This is the finite state machine for the game.
"""

import pyb
import utime
import random
import machine 

LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin(pyb.Pin.cpu.C13)
tim2 = pyb.Timer(2, freq = 20000)
LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)



class LightPattern:
    

    ## State Constants
    S0_INIT      = 0
    S1_LED1      = 1
    S2_USR1      = 2
    S3_LED2      = 3
    S4_USR2      = 4
    S5_SUCCESS   = 5
    S6_FAIL      = 6
    S7_END       = 7
    S8_CHECK     = 8
    
    def __init__(self):
        '''    
       @brief   This function initializes the class Light Pattern
       @details Variables required are set in this stage, and any variable that 
                needs to be kept through rounds is initialized here
       '''
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        # self.DBG_flag = DBG_flag
        
        ## Number of Patterns to Run
        self.wins = list()
        self.losses = list()
        self.success = 0
        self.gamelength = 10
        self.button_push = False
        self.difftime = 1500 # time user has to react to inputs
        self.press = 0
        self.presstime = 0
        self.usrtime = 10000
        self.complete = False #Sets complete varable for moving to next press in user pattern
        self.falltime = 0
        pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_RISING_FALLING,pull=pyb.Pin.PULL_NONE, callback= self.fpress)
    
    def run(self):
        '''
        @brief This function runs the various states of the Finite State Machine
        @details This function has the 8 different states of the FSM. It handles
                 calling to other functions in the states as well as setting 
                 variables that need to be changed on each iteration of the game.
        '''
        if self.state == self.S0_INIT:
            self.difficulty = 2
            self.patterngen()
            self.i = 0 
            self.usrpattern = list()
            print(self.pattern)
            self.start = utime.ticks_ms()
            self.transitionTo(self.S1_LED1)
            self.presstime = 0
            
        
        elif self.state == self.S1_LED1:
            #for x in range(0,len(self.pattern)/self.difficulty):
            self.lights(self.i)
            if utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                if self.i < (len(self.pattern)/self.difficulty)-1:
                    self.i = self.i + 1
                    self.start = utime.ticks_ms()
                else :
                    self.i = 0
                    self.start = utime.ticks_ms()
                    self.transitionTo(self.S2_USR1)
                    print('Start Entering Your Pattern')
            
        
        elif self.state == self.S2_USR1:
            #if user passes set difficulty to 1 to prep longer lights range
            self.upattern(self.i)
            if self.complete == True:
                if self.i < (len(self.pattern)/self.difficulty)-1:
                    self.i = self.i + 1
                    self.complete = False
                    self.start = utime.ticks_ms()
                else:
                    self.i = 0
                    self.complete = False
                    self.transitionTo(self.S8_CHECK)
                    print('Let me check that!')
                    self.start = utime.ticks_ms()
                    
                
        
        elif self.state == self.S3_LED2:
            self.lights(self.i)
            if utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                if self.i < len(self.pattern)-1:
                    self.i = self.i + 1
                    self.start = utime.ticks_ms()
                else:
                    self.i = 0
                    self.start = utime.ticks_ms()
                    self.usrpattern.clear()
                    self.transitionTo(self.S4_USR2)
                    print('Start Entering Your Pattern')
        
        elif self.state == self.S4_USR2:
            self.upattern(self.i)
            if self.complete == True:
                if self.i < (len(self.pattern))-1:
                    self.i = self.i + 1
                    self.complete = False
                    self.start = utime.ticks_ms()
                    self.presstime = 0
                else:
                    self.i = 0
                    self.complete = False
                    self.transitionTo(self.S8_CHECK)
                    print('Let me check that!')
                    self.start = utime.ticks_ms()
        
        elif self.state == self.S5_SUCCESS:
            if self.success == 1:
                self.transitionTo(self.S3_LED2)
                print('Congradulations! Moving on to round 2!')
            elif self.success == 2:
                print('Congratulations! You won.')
                self.wins.append(1)
                print('Your win to loss ratio is  ' + str(len(self.wins)) + '  wins  ' + str(len(self.losses))+ '  losses')
                answer = input(('Would you like to play another game?'))
                if answer == 'yes':
                    self.transitionTo(self.S0_INIT)
                else:
                    self.transitionTo(self.S7_END)
                
        
        elif self.state == self.S6_FAIL:
            self.losses.append(1)
            print('Your win to loss ratio is  ' + str(len(self.wins)) + '  wins  ' + str(len(self.losses))+ '  losses')
            answer = input(('Would you like to play another game?'))
            if answer == 'yes':
                self.transitionTo(self.S0_INIT)
            else:
                self.transitionTo(self.S7_END)
                
        
        elif self.state == self.S7_END:
            print('End Of Game')
            machine.soft_reset()
            pass
        
        elif self.state == self.S8_CHECK:
            if self.usrpattern == self.pattern[0:len(self.usrpattern)]:
                self.success = self.success + 1
                self.transitionTo(self.S5_SUCCESS)
            else:
                self.transitionTo(self.S6_FAIL)
                print('I am sorry, that is incorrect')
        
    def lights(self,i):
            '''
            @brief This function displays the LED pattern created.
            @details This function displays the LED pattern that is created by 
                     the pattern generator. It takes the index to run and if 
                     the input is a 1 it does a short blink, if the input is a 
                     2 it does a long blink.
            @param i is the index that the lights should run, based off the random
                   pattern that is generated.
            '''
            if self.pattern[i] == 1: #Short Press
                if 100 <= utime.ticks_diff(utime.ticks_ms(),self.start) <= 400:
                    blink = 100
                    LEDINT.pulse_width_percent(blink)
                elif utime.ticks_diff(utime.ticks_ms(),self.start) <= self.difftime:
                    blink = 0
                    LEDINT.pulse_width_percent(blink)
            
            elif self.pattern[i] == 2: # Long Press
                if 50 <= utime.ticks_diff(utime.ticks_ms(),self.start) <= 1000:
                    blink = 100
                    LEDINT.pulse_width_percent(blink)
                elif utime.ticks_diff(utime.ticks_ms(),self.start) <= self.difftime:
                     blink = 0
                     LEDINT.pulse_width_percent(blink)
    
    def transitionTo(self, newState):
        '''
        @brief This function moves the program into a new state
        @param newState is the new state that is transfered to.
        '''
        
        self.state = newState
    
    
    def fpress(self,IRQ_SRC):  #Press
            '''
            @brief This function handles the user button press
            @details This function handles the user button presses and sets
                     the presstime for use later in the program.
            '''
            self.button_push = True
            self.press = self.press + 1 
            if self.press == 1:
                self.falltime = utime.ticks_ms()
            elif self.press == 2:
                    self.presstime = utime.ticks_diff(utime.ticks_ms(),self.falltime)
                    self.press = 0
                    self.button_push = False 
   
    
    def patterngen(self): # Pattern Generator 
        '''
        @brief This function creates a random pattern of 1's and 2's.
        '''
        self.pattern = list()
        for i in range(0,self.gamelength):
            self.pattern.append(random.randint(1,2)) 
            
    
    def upattern(self, i): # User Pattern Monitor
        '''
        @brief This function interprets the user button inputs
        @details This function interprets the user button inputs as either long
                 or short presses. If it is a short press the user list is 
                 appended with a one. If it is a long press the user list is 
                 appended with a two. If the user presses too long or too 
                 short for that index, it appends a 3 to the userlist 
                 indicating a wrong answer.
        @param i is the index the user is currently on.
        '''
        if self.pattern[i] == 1: # short press
            if  50 <= self.presstime <= 400:
                self.usrpattern.append(1)
                self.complete = True
                self.presstime = 0
            elif utime.ticks_diff(utime.ticks_ms(),self.start) >= self.usrtime or self.presstime >400:
                self.usrpattern.append(3)
                self.complete = True
                self.presstime = 0
       
        elif self.pattern[i] == 2: # long press
            if  400 < self.presstime <= 2000:
                self.usrpattern.append(2)
                self.complete = True
                self.presstime =0
            elif utime.ticks_diff(utime.ticks_ms(),self.start) >= self.usrtime or self.presstime >2000 or 400 > self.presstime > 0 :
                self.usrpattern.append(3)
                self.complete = True
                self.presstime = 0
        elif self.i == 9:
            pass
        