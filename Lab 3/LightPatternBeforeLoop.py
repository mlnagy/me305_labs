# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 08:44:02 2021

@author: matth
"""

import pyb
import utime
import random

LED = pyb.Pin(pyb.Pin.cpu.A5)
Button = pyb.Pin (pyb.Pin.cpu.C13)
tim2 = pyb.Timer(2, freq = 20000)
LEDINT = tim2.channel(1, pyb.Timer.PWM, pin = LED)
difftime = 500 # time user has to react to inputs

class LightPattern:
    

    ## State Constants
    S0_INIT      = 0
    S1_LED1      = 1
    S2_USR1      = 2
    S3_LED2      = 3
    S4_USR2      = 4
    S5_SUCCESS   = 5
    S6_FAIL      = 6
    S7_END       = 7
    
    def __init__(self, DBG_flag):
       ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Number of Patterns to Run
        self.gamelength = 10
              
        ButtonInt = pyb.ExtInt(Button, mode=pyb.ExtInt.IRQ_RISING_FALLING,pull=pyb.Pin.PULL_NONE, callback= self.fpress)
    
    def run(self):
        if self.state == self.S0_INIT:
            #run pattern generator 
            #set difficulty to 2 to prep the lights range
            self.difficulty = 2
            self.patterngen
            i = 0 
            self.start = self.utime.ticks_ms()
            self.transitionTo(self.S1_LED)
        
        elif self.state == self.S1_LED1:
            #for x in range(0,len(self.pattern)/self.difficulty):
            self.lights(i)
            if utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                if i < len(self.pattern/self.difficulty):
                    i = i + 1
                    self.start = self.utime.ticks_ms()
                elif i == len(self.pattern):
                    i = 0
                    self.utime.ticks_ms()
                    self.transitionTo(self.S2_USR1)
            
        
        elif self.state == self.S2_USR1:
            #if user passes set difficulty to 1 to prep longer lights range
            self.usrpattern(i)
            if utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                if i < len(self.pattern/self.difficulty):
                    i = i + 1
                    self.start = self.utime.ticks_ms()
                elif i == len(self.pattern):
                    i = 0
                    self.transitionTo(self.S2_USR1)
                    self.utime.ticks_ms()
                
        
        elif self.state == self.S3_LED2:
            pass
        
        elif self.state == self.S4_USR2:
            pass
        
        elif self.state == self.S5_SUCCESS:
            pass
        
        elif self.state == self.S6_FAIL:
            pass
        
        elif self.state == self.S7_END:
            pass
        
    def lights(self,i):
            
            if self.pattern[i] == 0: # No light
                if utime.ticks_diff(utime.ticks_ms(),self.start) <= self.difftime:
                    blink = 0
                    LEDINT.pulse_width_percent(blink)
                elif utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                     pass      
            
            elif self.pattern[i] == 1: #Short Press
                if 100 <= utime.ticks_diff(utime.ticks_ms(),self.start) <= 300:
                    blink = 100
                    LEDINT.pulse_width_percent(blink)
                elif utime.ticks_diff(utime.ticks_ms(),self.start) <= self.difftime:
                    blink = 0
                    LEDINT.pulse_width_percent(blink)
            
            elif self.pattern[i] == 2: # Long Press
                if 50 <= utime.ticks_diff(utime.ticks_ms(),self.start) <= 450:
                    blink = 100
                    LEDINT.pulse_width_percent(blink)
                elif utime.ticks_diff(utime.ticks_ms(),self.start) <= self.difftime:
                     blink = 0
                     LEDINT.pulse_width_percent(blink)
    
    def transitionTo(self, newState):
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
    
    
    def fpress(self): # Falling Press
            self.button_push = True
            self.press = self.press + 1 
            self.falltime = utime.ticks_ms()
            if self.press == 2:
                    self.presstime = utime.ticks_diff(utime.ticks_ms(),self.falltime)
                    self.press = 0
                    self.button_push = False # is this needed here?
   
    
    def patterngen(self): # Pattern Generator 
        self.pattern = list()
        for i in range(0,self.gamelength):
            self.pattern.append(random.randint(0,2)) 
            
    
    def upattern(self, i): # User Pattern Monitor
        self.usrpattern = list()
        if self.pattern[i] == 0:
            if self.button_push == False and utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                self.usrpattern[i] = 0
            elif utime.ticks_diff(utime.ticks_ms(),self.start) >= self.difftime:
                self.usrpattern[i] = 3
        
        if self.pattern[i] == 1: # short press
            if self.button_push == True and self.presstime <= 250:
                self.usrpattern[i] = 1
            else:
                self.usrpattern[i] = 3
                
        if self.pattern[i] == 2: # long press
            if self.button_push == True and 250 < self.presstime <= 450:
                self.usrpattern[i] = 2
            else:
                self.usrpattern[i] = 3
        