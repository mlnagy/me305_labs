# -*- coding: utf-8 -*-
"""@file SimonSays.py 
@brief This program runs the SimonSays program.
@details This program runs the SimonSays program. More details on the inner workings 
         can be found in the LightPattern.py documentation.
         See code here: https://bitbucket.org/mlnagy/me305_labs/src/master/Lab%203/SimonSays.py
@author Matthew Nagy
@date Created on Thu Feb 4 09:40:55 2021
@copyright Licence Info
"""
import utime 
from LightPattern import LightPattern
run = 1
waitime  = utime.ticks_ms()


if __name__ == "__main__":
    task1 = LightPattern()
    while True:
        if utime.ticks_diff(utime.ticks_ms(), waitime) < 5000:
            if run == 1:
                print("This is Simon Says! Please watch the flashing LED for the pattern to follow")
                print('Press the blue button quicly for a quick flash or hold it down for a long flash')
                print('The game will start in 5 seconds!')
                run = run +1
        else:
                task1.run()
            
   
        